# zola

Static site generator. https://getzola.org

* https://staticgen.com/zola
* https://staticsitegenerators.net/

---

* https://github.com/getzola/zola
* [*Installation*](https://www.getzola.org/documentation/getting-started/installation/)
* [*GitLab Pages*](https://www.getzola.org/documentation/deployment/gitlab-pages/)
* Repology.org: [zola](https://repology.org/project/zola/versions)
* Alpine Linux: [zola](https://pkgs.alpinelinux.org/package/edge/community/x86_64/zola)
